---
#title: 个人简介
date: 2019-05-26 23:52:10
comments: false
type: "about"
---
## 职业
一个多年从事编程的深度学习爱好者    

> 百度公司 &emsp;&emsp;&emsp;&emsp;&emsp; 推荐技术平台部 &emsp;&emsp;  机器学习算法实习    
> 中国科学院大学   &emsp;&emsp;  电子与通信工程   &emsp;&emsp;   硕士（**入学top3**）

## 项目                                                             
- 短视频内容理解与推荐（2019ICME Grand Challenge)    
**心得**：改进算法将item属性和二阶结果进行cross交叉得到三阶组合矩阵，以解决三阶组合过于复杂的问题，同时与一阶cross结果融合，方法有待实验证明。
- 自动生成文本标题（2018ByteCup比赛题目)    
**心得**：Pytorch代码全部由自己根据算法原理所写，能够实现算法的原理，有一定的主题词提取效果，但是对于神经网络层与层之间的特征处理能力有待加强，应该多看论文，了解所使用的方法的原理，以及如何改进算法以获得更好结果。
- 基于深层学习方法的高频交易数据波动预测     
**心得**：前期因为数据震荡过大导致网络一直处于欠拟合状态，后来对数据进行多次平滑实验，才有好点的结果，这是接触深度学习的第一个项目，深度学习的使用感触很深    

## 获奖
> 中国科学院大学三好学生    
> 中国科学院大学优秀学生干部     
> 大学英语六级     

## 论文&专利
1. CCC-会议论文
    [e-print](https://arxiv.org/abs/1903.01564)

## 参与社区
> CSDN: [https://blog.csdn.net/univeryinli](https://blog.csdn.net/univeryinli)    
> cnblog: [https://home.cnblogs.com/u/univeryinli/](https://home.cnblogs.com/u/univeryinli/)      
> github: [https://github.com/univeryinli](https://github.com/univeryinli)     

## 爱好
喜欢骑行
![青海骑行](https://s2.ax1x.com/2019/06/06/Vdtt9P.jpg)

喜欢旅游
![甘肃自驾游](https://s2.ax1x.com/2019/06/06/VdtN1f.jpg)